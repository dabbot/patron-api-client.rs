use crate::{
    error::Result,
    model::PatronUser,
};
use futures::{
    compat::Future01CompatExt,
};
use log::debug;
use reqwest::r#async::Client as ReqwestClient;
use std::sync::Arc;

pub struct Client {
    host: String,
    https: bool,
    inner: Arc<ReqwestClient>,
}

impl Client {
    pub fn new(
        client: Arc<ReqwestClient>,
        https: bool,
        host: impl Into<String>,
    ) -> Self {
        Self {
            host: host.into(),
            inner: client,
            https,
        }
    }

    pub async fn patron<'a>(
        &'a self,
        user_id: u64,
    ) -> Result<PatronUser> {
        debug!("Preparing patron API user request");
        let url = http_client_base::url(
            self.https,
            &self.host,
            format!("/v1/users/{}", user_id),
            None,
        )?;
        debug!("Created url: {}", url);

        debug!("Sending patron API user request");
        let mut resp = await!(self
            .inner
            .get(url)
            .send()
            .compat())?;
        debug!("Got patron api user response; deserializing");

        let model = await!(resp.json().compat())?;
        debug!("Deserialized patron API user response");

        Ok(model)
    }
}
