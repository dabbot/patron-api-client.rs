#![feature(async_await, await_macro, futures_api)]

pub mod model;

mod client;
mod error;

pub use self::{
    client::Client as PatronClient,
    error::{Error, Result},
};
